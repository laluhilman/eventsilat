package com.evensilat.eventsilat

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText

class ESInputArenaActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_arena)

        var ipAddress  = findViewById<EditText>(R.id.arenaipAdress)
        var arenaName  = findViewById<EditText>(R.id.aarenarenaNmae)
        var submitButton  = findViewById<Button>(R.id.submitButton)



        var preference = SharedPreference()

        if(preference.getValue(this, ESConstant.arenaIpAddress)!=null){
            ipAddress.setText( preference.getValue(this, ESConstant.arenaIpAddress))
        }

        if(preference.getValue(this, ESConstant.arenaArenaName)!=null){
            arenaName.setText( preference.getValue(this, ESConstant.arenaArenaName))
        }

        if(preference.getValue(this, ESConstant.arenaIpAddress)!=null && preference.getValue(this, ESConstant.arenaArenaName)!=null ){

            var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
            toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/arenascreen/index.php?arenaName="+ arenaName.text.toString())
            startActivity(toWebViewActivity)
        }

        submitButton.setOnClickListener {
            if(ipAddress.text.toString().length==0 || arenaName.text.toString().length==0 )
                ipAddress.setError("Please Input Url")
            else{

                preference.save(this, ESConstant.arenaIpAddress, ipAddress.text.toString())
                preference.save(this, ESConstant.arenaArenaName, arenaName.text.toString())

                var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
                toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/arenascreen/index.php?arenaName="+ arenaName.text.toString())
                startActivity(toWebViewActivity)


            }

        }



    }


}