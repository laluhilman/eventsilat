package com.evensilat.eventsilat

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText

class ESInputTimerActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer)

        var ipAddress  = findViewById<EditText>(R.id.timeripAdress)
        var arenaName  = findViewById<EditText>(R.id.timerarenaNmae)
        var submitButton  = findViewById<Button>(R.id.submitButton)


        var preference = SharedPreference()

        if(preference.getValue(this, ESConstant.timerIpAddress)!=null){
            ipAddress.setText( preference.getValue(this, ESConstant.timerIpAddress))
        }

        if(preference.getValue(this, ESConstant.timerArenaName)!=null){
            arenaName.setText( preference.getValue(this, ESConstant.timerArenaName))
        }

        if(preference.getValue(this, ESConstant.timerIpAddress)!=null && preference.getValue(this, ESConstant.timerArenaName)!=null){
            var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
            toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/timer/index.php?arenaName="+ arenaName.text.toString())
            startActivity(toWebViewActivity)
        }

        submitButton.setOnClickListener {
            if(ipAddress.text.toString().length==0 || arenaName.text.toString().length==0 )
                ipAddress.setError("Please Input Url")
            else{

                preference.save(this, ESConstant.timerIpAddress, ipAddress.text.toString())
                preference.save(this, ESConstant.timerArenaName, arenaName.text.toString())

                //http://192.168.0.16/eventsilat/arenascreen/index.php?arenaName=1

                var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
                toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/timer/index.php?arenaName="+ arenaName.text.toString())
                startActivity(toWebViewActivity)


            }

        }



    }


}