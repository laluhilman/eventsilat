package com.evensilat.eventsilat

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText

class ESInputJudgeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_judge)

        var ipAddress  = findViewById<EditText>(R.id.judgeipAdress)
        var arenaName  = findViewById<EditText>(R.id.judgearenaNmae)
        var judgeNumber  = findViewById<EditText>(R.id.judgejudgeNumber)
        var submitButton  = findViewById<Button>(R.id.submitButton)


        var preference = SharedPreference()

        if(preference.getValue(this, ESConstant.judgeIpAddress)!=null){
            ipAddress.setText( preference.getValue(this, ESConstant.judgeIpAddress))
        }

        if(preference.getValue(this, ESConstant.judgeArenaName)!=null){
            arenaName.setText( preference.getValue(this, ESConstant.judgeArenaName))
        }

        if(preference.getValue(this, ESConstant.judgeJudgeNumber)!=null){
            judgeNumber.setText( preference.getValue(this, ESConstant.judgeJudgeNumber))
        }


        if(preference.getValue(this, ESConstant.judgeIpAddress)!=null && preference.getValue(this, ESConstant.judgeArenaName)!=null &&
            preference.getValue(this, ESConstant.judgeJudgeNumber)!=null  ){


            var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
            toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/juri/index.php?arenaName="+ arenaName.text.toString()+"&judgeNumber"+ judgeNumber.text.toString())
            startActivity(toWebViewActivity)
        }

        submitButton.setOnClickListener {
            if(ipAddress.text.toString().length==0 || arenaName.text.toString().length==0 || judgeNumber.text.toString().length==0 )
                ipAddress.setError("Please Input Url")
            else{

                preference.save(this, ESConstant.judgeIpAddress, ipAddress.text.toString())
                preference.save(this, ESConstant.judgeArenaName, arenaName.text.toString())
                preference.save(this, ESConstant.judgeJudgeNumber, judgeNumber.text.toString())

                //http://192.168.0.16/eventsilat/arenascreen/index.php?arenaName=1

                var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
                toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/juri/index.php?arenaName="+ arenaName.text.toString()+"&judgeNumber"+ judgeNumber.text.toString())
                startActivity(toWebViewActivity)


            }

        }



    }

}