package com.evensilat.eventsilat

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText

class ESInputAthleteActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_athlete)

        var ipAddress  = findViewById<EditText>(R.id.athleteipAdress)
        var screenNumber  = findViewById<EditText>(R.id.athletescreenNumber)
        var totalScreenNumber  = findViewById<EditText>(R.id.athletetotalScreenNumber)
        var submitButton  = findViewById<Button>(R.id.submitButton)



        var preference = SharedPreference()

        if(preference.getValue(this, ESConstant.athleteIpAddress)!=null){
            ipAddress.setText( preference.getValue(this, ESConstant.athleteIpAddress))
        }

        if(preference.getValue(this, ESConstant.athleteScreenNumber)!=null){
            screenNumber.setText( preference.getValue(this, ESConstant.athleteScreenNumber))
        }

        if(preference.getValue(this, ESConstant.athleteTotalScreeen)!=null){
            totalScreenNumber.setText( preference.getValue(this, ESConstant.athleteTotalScreeen))
        }

        if(preference.getValue(this, ESConstant.athleteIpAddress)!=null &&
            preference.getValue(this, ESConstant.athleteScreenNumber)!=null&&
            preference.getValue(this, ESConstant.athleteTotalScreeen)!=null ){
            var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
            toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/athleteroom/index.php?screen_number="+  screenNumber.text.toString() + "&total_screen_number="+ totalScreenNumber.text.toString())
            startActivity(toWebViewActivity)

        }



        submitButton.setOnClickListener {
            if(ipAddress.text.toString().length==0 || screenNumber.text.toString().length==0 || totalScreenNumber.text.toString().length==0 )
                ipAddress.setError("Please Input Url")
            else{

                preference.save(this, ESConstant.athleteIpAddress, ipAddress.text.toString())
                preference.save(this, ESConstant.athleteScreenNumber, screenNumber.text.toString())
                preference.save(this, ESConstant.athleteTotalScreeen, totalScreenNumber.text.toString())

                var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
                toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/athleteroom/index.php?screen_number="+  screenNumber.text.toString() + "&total_screen_number="+ totalScreenNumber.text.toString())
                startActivity(toWebViewActivity)


            }

        }



    }



}