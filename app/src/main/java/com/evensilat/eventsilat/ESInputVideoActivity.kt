package com.evensilat.eventsilat

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText

class ESInputVideoActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        var ipAddress  = findViewById<EditText>(R.id.videoipAdress)
        var arenaName  = findViewById<EditText>(R.id.videoarenaNmae)
        var submitButton  = findViewById<Button>(R.id.submitButton)



        var preference = SharedPreference()

        if(preference.getValue(this, ESConstant.videoIpAddress)!=null){
            ipAddress.setText( preference.getValue(this, ESConstant.videoIpAddress))
        }

        if(preference.getValue(this, ESConstant.videoArenaName)!=null){
            arenaName.setText( preference.getValue(this, ESConstant.videoArenaName))
        }

        if(preference.getValue(this, ESConstant.videoIpAddress)!=null && preference.getValue(this, ESConstant.videoArenaName)!=null ){

            var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
            toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/videoscreen/index.php?arenaName="+ arenaName.text.toString())
            startActivity(toWebViewActivity)
        }

        submitButton.setOnClickListener {
            if(ipAddress.text.toString().length==0 || arenaName.text.toString().length==0 )
                ipAddress.setError("Please Input Url")
            else{

                preference.save(this, ESConstant.videoIpAddress, ipAddress.text.toString())
                preference.save(this, ESConstant.videoIpAddress, arenaName.text.toString())

                var toWebViewActivity = Intent(this, ESWebViewActivity::class.java)
                toWebViewActivity.putExtra("url", "http://"+ ipAddress.text.toString()+"/eventsilat/arenascreen/index.php?arenaName="+ arenaName.text.toString())
                startActivity(toWebViewActivity)


            }

        }



    }


}