package com.evensilat.eventsilat

class ESConstant {
    companion object {
        val arenaIpAddress = "arena_ip_address"
        val arenaArenaName = "arena_arena_name"
        val arenaJudgeNumber = "arena_judge_number"
        val athleteIpAddress = "athlete_ip_address"
        val athleteScreenNumber = "athlete_screen_number"
        val athleteTotalScreeen = "athlete_total_screen"
        val judgeIpAddress = "judge_ip_address"
        val judgeArenaName = "judge_arena_name"
        val judgeJudgeNumber = "judge_judge_number"
        val timerIpAddress = "timer_ip_address"
        val timerArenaName = "timer_arena_name"
        val videoIpAddress = "video_ip_address"
        val videoArenaName = "video_arena_name"

        val ipNumber = "192.168.0."
    }
}