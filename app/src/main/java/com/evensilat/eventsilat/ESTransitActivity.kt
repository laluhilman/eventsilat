package com.evensilat.eventsilat

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.webkit.WebChromeClient
import android.webkit.WebSettings


class ESTransitActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_web_view)

        if(BuildConfig.TYPE == "ESArena"){
            var toArena =  Intent(this, ESInputArenaActivity::class.java);
            startActivity(toArena)
        } else if(BuildConfig.TYPE == "ESJudge"){
            var toArena =  Intent(this, ESInputJudgeActivity::class.java);
            startActivity(toArena)
        } else if(BuildConfig.TYPE == "ESTimer"){
            var toArena =  Intent(this, ESInputTimerActivity::class.java);
            startActivity(toArena)
        } else if(BuildConfig.TYPE == "ESAthlete"){
            var toArena =  Intent(this, ESInputAthleteActivity::class.java);
            startActivity(toArena)
        } else if(BuildConfig.TYPE == "ESVideo"){
            var toArena =  Intent(this, ESInputVideoActivity::class.java);
            startActivity(toArena)
        }

    }




    override fun onBackPressed() {

    }
}