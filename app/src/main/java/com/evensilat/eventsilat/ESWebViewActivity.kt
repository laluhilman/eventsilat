package com.evensilat.eventsilat

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.webkit.WebChromeClient
import android.webkit.WebSettings


class ESWebViewActivity : AppCompatActivity() {


    lateinit var browser: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_web_view)
        browser = findViewById(R.id.webview);
        getWindow().setFeatureInt( Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

        browser.settings.loadWithOverviewMode = true
        browser.settings.useWideViewPort = true
        browser.settings.loadsImagesAutomatically = true
        browser.settings.javaScriptEnabled = true

        browser.setWebViewClient(MyBrowser())





        browser.loadUrl(intent.getStringExtra("url"))
        Log.d("Debug", intent.getStringExtra("url"))
    }

    private inner class MyBrowser : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }



    override fun onBackPressed() {

    }
}