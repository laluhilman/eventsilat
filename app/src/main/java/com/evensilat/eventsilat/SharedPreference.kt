package com.evensilat.eventsilat

import android.R.id.edit
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences


class SharedPreference {

    fun save(context: Context, key: String, value: String) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE) //1
        editor = settings.edit() //2

        editor.putString(key, value) //3

        editor.commit() //4
    }

    fun getValue(context: Context, key :String): String? {
        val settings: SharedPreferences
        val text: String?

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        text = settings.getString(key, null)
        return text
    }

    fun clearSharedPreference(context: Context) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        editor = settings.edit()

        editor.clear()
        editor.commit()
    }

    fun removeValue(context: Context) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        editor = settings.edit()

        editor.remove(PREFS_KEY)
        editor.commit()
    }

    companion object {

        val PREFS_NAME = "AOP_PREFS"
        val PREFS_KEY = "AOP_PREFS_String"
    }
}